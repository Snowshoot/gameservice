import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val reactorKafka = "1.3.23"
val springKafka = "3.3.3"
val kotlinLogging = "7.0.4"
val kotest = "5.9.1"
val kotestContainers = "2.0.2"
val kotestSpring = "1.3.0"
val jackson = "2.18.2"
val springMongo = "4.4.3"
val springSecurity = "6.4.3"
val testContainers = "1.20.4"
val wiremock = "4.2.0"
val mockk = "1.13.16"

plugins {
    id("org.springframework.boot") version "3.4.2"
    id("io.spring.dependency-management") version "1.1.7"
    id("org.jetbrains.kotlin.plugin.allopen") version "2.1.10"
    kotlin("jvm") version "2.1.10"
    kotlin("plugin.spring") version "2.1.10"
}

group = "com.snow"
version = "1.0.0"

sourceSets {
    create("stubs") {
        compileClasspath += sourceSets["main"].output
        runtimeClasspath += sourceSets["main"].output
    }
    create("integrationTest") {
        compileClasspath += sourceSets["stubs"].output
        runtimeClasspath += sourceSets["stubs"].output
        compileClasspath += sourceSets["main"].output
        runtimeClasspath += sourceSets["main"].output
    }
    named("test") {
        compileClasspath += sourceSets["stubs"].output
        runtimeClasspath += sourceSets["stubs"].output
    }
}

configurations {
    named("integrationTestImplementation") {
        extendsFrom(configurations.testImplementation.get())
    }
    named("stubsImplementation") {
        extendsFrom(configurations.implementation.get())
    }
}


val integrationTest by tasks.registering(Test::class) {
    testClassesDirs = sourceSets["integrationTest"].output.classesDirs
    classpath = sourceSets["integrationTest"].runtimeClasspath
}

java {
    sourceCompatibility = JavaVersion.VERSION_21
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb-reactive")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jackson")
    implementation("io.projectreactor.kafka:reactor-kafka:$reactorKafka")
    implementation("org.springframework.kafka:spring-kafka:$springKafka")
    implementation("org.springframework.data:spring-data-mongodb:$springMongo")
    implementation("io.github.oshai:kotlin-logging-jvm:$kotlinLogging")
    implementation("org.springframework.security:spring-security-oauth2-resource-server:$springSecurity")
    implementation("org.springframework.security:spring-security-oauth2-jose:$springSecurity")
    testImplementation("io.kotest:kotest-runner-junit5-jvm:$kotest")
    testImplementation("io.kotest:kotest-assertions-core-jvm:$kotest")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("io.projectreactor:reactor-test")
    testImplementation("io.kotest.extensions:kotest-extensions-spring:$kotestSpring")
    testImplementation("io.mockk:mockk:$mockk")
    "integrationTestImplementation"("org.springframework.boot:spring-boot-testcontainers")
    "integrationTestImplementation"("org.testcontainers:junit-jupiter")
    "integrationTestImplementation"("org.testcontainers:mongodb")
    "integrationTestImplementation"("org.testcontainers:testcontainers:$testContainers")
    "integrationTestImplementation"("org.testcontainers:junit-jupiter:$testContainers")
    "integrationTestImplementation"("org.testcontainers:kafka:$testContainers")
    "integrationTestImplementation"("org.testcontainers:mongodb:$testContainers")
    "integrationTestImplementation"("io.kotest.extensions:kotest-extensions-testcontainers:$kotestContainers")
    "integrationTestImplementation"("org.springframework.security:spring-security-test:$springSecurity")
    "integrationTestImplementation"("org.springframework.cloud:spring-cloud-contract-wiremock:$wiremock")
}

tasks.withType<KotlinCompile> {
    compilerOptions {
        jvmTarget = JvmTarget.JVM_21
        freeCompilerArgs.add("-Xjsr305=strict")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}