package com.snow.gameservice.config.auth

import com.nimbusds.jose.Algorithm
import com.nimbusds.jose.HeaderParameterNames.KEY_ID
import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.crypto.RSASSASigner
import com.nimbusds.jose.jwk.KeyUse
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator
import com.nimbusds.jwt.JWTClaimsSet
import com.nimbusds.jwt.SignedJWT
import com.snow.gameservice.config.WiremockConfig
import java.lang.String.format
import java.time.Instant
import java.util.*

object AuthGenerator {
    private val rsaKey = RSAKeyGenerator(2048)
        .keyUse(KeyUse.SIGNATURE)
        .algorithm(Algorithm("RS256"))
        .keyID(KEY_ID)
        .generate()

    private val rsaPublicJWK = rsaKey.toPublicJWK()
    val jwk = format("{\"keys\": [%s]}", rsaPublicJWK.toJSONString())

    private val claims = JWTClaimsSet.Builder()
        .subject("testuser")
        .issuer("http://localhost:${WiremockConfig.PORT}/keycloak/realms/Gameservice")
        .claim("resource_access", mapOf("gameservice" to mapOf("roles" to listOf("user"))))
        .expirationTime(Date.from(Instant.now().plusSeconds(60)))
        .build()

    val jws = SignedJWT(
        JWSHeader.Builder(JWSAlgorithm.RS256).keyID(rsaKey.keyID).build(),
        claims
    ).apply {
        sign(RSASSASigner(rsaKey))
    }.serialize()
}