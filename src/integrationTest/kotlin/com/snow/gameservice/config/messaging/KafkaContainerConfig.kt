package com.snow.gameservice.config.messaging

import org.testcontainers.containers.startupcheck.IsRunningStartupCheckStrategy
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.kafka.ConfluentKafkaContainer
import org.testcontainers.utility.DockerImageName

object KafkaContainerConfig {
    fun initialize() {
        kafkaContainer.start()
        setProperties()
    }

    @Container
    private val kafkaContainer: ConfluentKafkaContainer =
        ConfluentKafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:latest"))
            .withStartupCheckStrategy(IsRunningStartupCheckStrategy())

    private fun setProperties() =
        bootstrapServer().let {
            System.setProperty("messaging.games.publisher.server", it)
            System.setProperty("messaging.games.receiver.server", it)
        }

    private fun String.trimPrefix() = this.substringAfter("PLAINTEXT://")

    fun bootstrapServer(): String = kafkaContainer.bootstrapServers.trimPrefix()
}