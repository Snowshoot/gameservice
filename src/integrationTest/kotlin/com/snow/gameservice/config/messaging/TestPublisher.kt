package com.snow.gameservice.config.messaging

import com.snow.gameservice.messaging.game.receiver.model.GameDataDownloaded
import java.util.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.reactive.asFlow
import org.apache.kafka.clients.producer.ProducerConfig.BOOTSTRAP_SERVERS_CONFIG
import org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG
import org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.kafka.support.serializer.JsonSerializer
import reactor.core.publisher.Flux
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions
import reactor.kafka.sender.SenderRecord

object TestPublisher {
    private val publisher: KafkaSender<String, GameDataDownloaded> =
        KafkaSender.create(
            SenderOptions.create(
                mapOf(
                    BOOTSTRAP_SERVERS_CONFIG to KafkaContainerConfig.bootstrapServer(),
                    KEY_SERIALIZER_CLASS_CONFIG to StringSerializer::class.java,
                    VALUE_SERIALIZER_CLASS_CONFIG to JsonSerializer::class.java,
                )
            )
        )

    suspend fun publish(gameDataDownloaded: GameDataDownloaded) = publisher.send(
        Flux.just(gameDataDownloaded.toSenderRecord())
    ).asFlow().collect()

    private fun GameDataDownloaded.toSenderRecord() =
        SenderRecord.create(
            "games",
            0,
            null,
            "${UUID.randomUUID()}",
            this,
            ""
        )
}