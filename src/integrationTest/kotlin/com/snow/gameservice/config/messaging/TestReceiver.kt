package com.snow.gameservice.config.messaging

import com.snow.gameservice.messaging.game.publisher.model.GameDataPullRequested
import com.snow.gameservice.messaging.game.receiver.config.GameReceiverConfig.Companion.PROJECT_CLASSES
import kotlinx.coroutines.reactive.asFlow
import org.apache.kafka.clients.consumer.ConsumerConfig.*
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.kafka.support.serializer.JsonDeserializer
import org.springframework.kafka.support.serializer.JsonDeserializer.TRUSTED_PACKAGES
import org.springframework.kafka.support.serializer.JsonDeserializer.TYPE_MAPPINGS
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverOptions
import java.util.*

object TestReceiver {
    private val receiver: KafkaReceiver<String, GameDataPullRequested> =
        KafkaReceiver.create(
            ReceiverOptions.create<String, GameDataPullRequested>(
                mapOf(
                    GROUP_ID_CONFIG to "update",
                    BOOTSTRAP_SERVERS_CONFIG to KafkaContainerConfig.bootstrapServer(),
                    KEY_DESERIALIZER_CLASS_CONFIG to StringDeserializer::class.java,
                    VALUE_DESERIALIZER_CLASS_CONFIG to JsonDeserializer::class.java,
                    AUTO_OFFSET_RESET_CONFIG to "earliest",
                    TRUSTED_PACKAGES to PROJECT_CLASSES,
                    TYPE_MAPPINGS to "gameDataPullRequested:com.snow.gameservice.messaging.game.publisher.model.GameDataPullRequested"
                )
            ).subscription(Collections.singleton("update"))
        )

    fun receive() = receiver.receive().asFlow()
}