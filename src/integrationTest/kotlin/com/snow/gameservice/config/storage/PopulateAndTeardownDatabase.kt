package com.snow.gameservice.config.storage

import com.mongodb.client.result.UpdateResult
import com.snow.gameservice.domain.followed.FollowedService
import com.snow.gameservice.domain.game.GameService
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.notification.NotificationService
import com.snow.gameservice.domain.review.ReviewService
import com.snow.gameservice.game.ApiGameStub
import com.snow.gameservice.review.ReviewStub
import com.snow.gameservice.storage.followed.model.FollowedDocument
import com.snow.gameservice.storage.game.model.GameDocument
import com.snow.gameservice.storage.notification.model.NotificationDocument
import com.snow.gameservice.storage.review.model.ReviewDocument
import io.kotest.core.listeners.TestListener
import io.kotest.core.test.TestCase
import io.kotest.core.test.TestResult
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.stereotype.Component

@Component
class PopulateAndTeardownDatabase(
    private val gameService: GameService,
    private val followedService: FollowedService,
    private val reviewService: ReviewService,
    private val notificationService: NotificationService,
    private val mongoTemplate: ReactiveMongoTemplate,
) : TestListener {
    val user = "testuser"
    val curator = "testcurator"

    var gameIds: List<String?>? = null
    var followedId: String? = null
    var reviewedId: String? = null
    var notificationId: String? = null

    override suspend fun beforeEach(testCase: TestCase) {
        gameIds =
            gameService.saveAll(ApiGameStub.collection).map { it.id() }.toList()
        gameIds?.let { ids ->
            if (ids.isNotEmpty())
                ids.first()?.let { gameId ->
                    followedService.follow(GameId(gameId),user).also { followedId = gameId }
                    reviewService.save(ReviewStub.create(curator, GameId(gameId))).also { reviewedId = gameId }
                    notificationService.queueNotifications(GameId(gameId), flowOf(user)).also { notificationId = gameId }
                }
            ids[1]?.let { gameId -> followedService.follow(GameId(gameId), "testollo").also { followedId = gameId } }
        }
    }

    override suspend fun afterEach(testCase: TestCase, result: TestResult) {
        with(mongoTemplate) {
            dropCollection(GameDocument::class.java).subscribe()
            dropCollection(FollowedDocument::class.java).subscribe()
            dropCollection(ReviewDocument::class.java).subscribe()
            dropCollection(NotificationDocument::class.java).subscribe()
        }
    }
}

private fun UpdateResult.id() = this.upsertedId?.asString()?.value