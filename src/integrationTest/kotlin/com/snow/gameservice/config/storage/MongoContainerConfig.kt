package com.snow.gameservice.config.storage

import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.junit.jupiter.Container

object MongoContainerConfig {
    fun initialize() {
        mongoDBContainer.start()
        setProperties()
    }

    @Container
    private val mongoDBContainer: MongoDBContainer = MongoDBContainer("mongo:latest")

    private fun setProperties() {
        System.setProperty("spring.data.mongodb.uri", mongoDBContainer.replicaSetUrl)
    }
}