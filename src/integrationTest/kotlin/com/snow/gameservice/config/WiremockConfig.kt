package com.snow.gameservice.config

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.aResponse
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.urlMatching
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.snow.gameservice.config.auth.AuthGenerator
import org.springframework.http.MediaType

object WiremockConfig {
    const val PORT: Int = 40407
    private val server = WireMockServer(WireMockConfiguration().port(PORT))

    fun initialize() {
        server.start()
        val url = server.baseUrl()
        System.setProperty("spring.security.oauth2.resourceserver.jwt.issuer-uri", "$url/keycloak/realms/Gameservice")
        System.setProperty("spring.security.oauth2.resourceserver.jwt.jwk-set-uri", "$url/keycloak/jwk")

        server.stubFor(
            get(urlMatching("/keycloak/jwk")).willReturn(
                aResponse()
                    .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                    .withBody(AuthGenerator.jwk)
            )
        )
    }
}