package com.snow.gameservice.config.util

import org.springframework.web.reactive.function.BodyInserters

fun Any.bodyInserter() = BodyInserters.fromValue(this)