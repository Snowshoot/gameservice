package com.snow.gameservice.config

import com.snow.gameservice.config.messaging.KafkaContainerConfig
import com.snow.gameservice.config.storage.MongoContainerConfig
import io.kotest.core.config.AbstractProjectConfig

object KotestConfig : AbstractProjectConfig() {
    override suspend fun beforeProject() {
        KafkaContainerConfig.initialize()
        MongoContainerConfig.initialize()
        WiremockConfig.initialize()
    }
}