package com.snow.gameservice.config

import com.snow.gameservice.user.TestUser
import org.springframework.boot.web.reactive.context.ReactiveWebApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.*
import org.springframework.stereotype.Component
import org.springframework.test.web.reactive.server.WebTestClient

@Component
class WebTestClientConfig {

    @Bean
    fun webTestClient(
        context: ReactiveWebApplicationContext,
    ): WebTestClient =
        WebTestClient.bindToApplicationContext(context)
            .apply(springSecurity())
            .configureClient()
            .build()
            .mutateWith(csrf())
}

fun WebTestClient.setUser(
    user: TestUser,
) =
    this.mutateWith(
        mockJwt()
            .jwt { it.claim("sub", user.userId) }
            .authorities(user.roles.map { SimpleGrantedAuthority("ROLE_${it}") })
    )
