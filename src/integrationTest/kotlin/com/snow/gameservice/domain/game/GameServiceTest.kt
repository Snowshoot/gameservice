package com.snow.gameservice.domain.game

import com.snow.gameservice.config.IntegrationTest
import com.snow.gameservice.config.storage.PopulateAndTeardownDatabase
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.string.beEmpty
import kotlinx.coroutines.flow.first

@IntegrationTest
class GameServiceTest(
    private val gameService: GameService,
    private val dbSetup: PopulateAndTeardownDatabase,
) : FeatureSpec({
    listeners(dbSetup)

    feature("Find games releasing today") {
        scenario("At least one game should be found") {
            gameService.findReleasingToday().first().value shouldNotBe beEmpty()
        }
    }
})