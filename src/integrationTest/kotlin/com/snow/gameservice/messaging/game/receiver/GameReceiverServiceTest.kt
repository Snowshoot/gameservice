package com.snow.gameservice.messaging.game.receiver

import com.snow.gameservice.config.IntegrationTest
import com.snow.gameservice.config.messaging.TestPublisher
import com.snow.gameservice.domain.game.GameService
import com.snow.gameservice.game.GameDataDownloadedStub
import com.snow.gameservice.game.GameSearchParamsStub
import com.snow.gameservice.messaging.game.receiver.model.GameDataDownloaded
import io.kotest.assertions.nondeterministic.eventually
import io.kotest.common.ExperimentalKotest
import io.kotest.core.coroutines.backgroundScope
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.core.test.testCoroutineScheduler
import io.kotest.matchers.collections.shouldNotBeEmpty
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import reactor.kafka.receiver.KafkaReceiver
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class, ExperimentalStdlibApi::class, ExperimentalKotest::class)
@IntegrationTest
class GameReceiverServiceTest(
    private val receiver: KafkaReceiver<String, GameDataDownloaded>,
    private val gameService: GameService,
) : FeatureSpec({
    feature("Receiving a gameDataDownloaded event").config(
        coroutineTestScope = true
    ) {
        val published = GameDataDownloadedStub.create(externalId = 6435456, title = "Kafka adventures")
        scenario("Published game should be received") {
            TestPublisher.publish(published)
            GameReceiverService(receiver, gameService, this.backgroundScope) // Starts listening on init

            testCoroutineScheduler.advanceUntilIdle()
            eventually(3.seconds) {
                gameService.findAll(GameSearchParamsStub.create(published.title).toDomain(), 0)
                    .toList()
                    .shouldNotBeEmpty()
            }
        }
    }
})