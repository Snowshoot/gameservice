package com.snow.gameservice.messaging.game.publisher

import com.snow.gameservice.config.IntegrationTest
import com.snow.gameservice.config.messaging.TestReceiver
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.string.beEmpty
import kotlinx.coroutines.flow.first

@IntegrationTest
class GameDataPullRequestPublisherServiceTest(
    private val gameDataPullRequestPublisherService: GameDataPullRequestPublisherServiceImpl,
) : FeatureSpec({
    feature("Publishing GameDataPullRequest") {
        gameDataPullRequestPublisherService.publish()

        scenario("Event should be pushed to kafka") {
            TestReceiver.receive().first().value().id shouldNotBe beEmpty()
        }
    }
})