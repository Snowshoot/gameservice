package com.snow.gameservice.scheduler

import com.snow.gameservice.config.IntegrationTest
import com.snow.gameservice.config.storage.PopulateAndTeardownDatabase
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.notification.NotificationService
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.collections.beEmpty
import io.kotest.matchers.shouldNotBe

@IntegrationTest
class NotificationSchedulerTest(
    private val notificationScheduler: NotificationScheduler,
    private val notificationService: NotificationService,
    private val dbSetup: PopulateAndTeardownDatabase,
) : FeatureSpec({
    listeners(dbSetup)

    feature("Queueing notifications") {
        val userId = "testollo"

        scenario("One game should be queued") {
            notificationScheduler.queueNotifications()

            notificationService.findNotifications(userId).collect {
                it shouldNotBe beEmpty<GameId>()
            }
        }
    }
})