package com.snow.gameservice.api.followed

import com.snow.gameservice.api.game.model.GameDto
import com.snow.gameservice.config.IntegrationTest
import com.snow.gameservice.config.setUser
import com.snow.gameservice.config.storage.PopulateAndTeardownDatabase
import com.snow.gameservice.user.TestUser
import io.kotest.core.spec.style.FeatureSpec
import org.springframework.test.web.reactive.server.WebTestClient

@IntegrationTest
class FollowedControllerTest(
    private val webTestClient: WebTestClient,
    private val dbSetup: PopulateAndTeardownDatabase,
) : FeatureSpec({
    listeners(dbSetup)
    val userClient = webTestClient.setUser(TestUser.user)

    feature("Get followed games") {
        scenario("User has access") {
            userClient
                .get()
                .uri {
                    it
                        .path("/followed")
                        .queryParam("page", 0)
                        .build()
                }
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(GameDto::class.java)
                .hasSize(1)
        }

        scenario("User without access") {
            webTestClient
                .get()
                .uri {
                    it
                        .path("/followed")
                        .queryParam("page", 0)
                        .build()
                }
                .exchange()
                .expectStatus()
                .isUnauthorized
        }
    }

    feature("Add game to followed") {
        scenario("User has access") {
            val gameId = dbSetup.gameIds?.first()
            userClient
                .post()
                .uri {
                    it
                        .path("/followed")
                        .queryParam("gameId", gameId)
                        .build()
                }
                .exchange()
                .expectStatus()
                .isOk
        }

        scenario("User has no access") {
            webTestClient
                .post()
                .uri {
                    it
                        .path("/followed")
                        .queryParam("gameId", "bebebe")
                        .build()
                }
                .exchange()
                .expectStatus()
                .isUnauthorized
        }
    }

    feature("Setting notification status") {
        scenario("User has access") {
            userClient
                .post()
                .uri {
                    it
                        .path("/followed/notifications")
                        .queryParam("notify", true)
                        .build()
                }
                .exchange()
                .expectStatus()
                .isOk
        }

        scenario("User has no access") {
            webTestClient
                .post()
                .uri {
                    it
                        .path("/followed/notifications")
                        .queryParam("notify", true)
                        .build()
                }
                .exchange()
                .expectStatus()
                .isUnauthorized
        }
    }

    feature("Unfollowing a game") {
        scenario("User has access") {
            val gameId = dbSetup.followedId
            userClient
                .delete()
                .uri {
                    it
                        .path("/followed")
                        .queryParam("gameId", gameId)
                        .build()
                }
                .exchange()
                .expectStatus()
                .isOk
        }

        scenario("User has no access") {
            webTestClient
                .delete()
                .uri {
                    it
                        .path("/followed")
                        .queryParam("gameId", "bebebe")
                        .build()
                }
                .exchange()
                .expectStatus()
                .isUnauthorized
        }
    }
})