package com.snow.gameservice.api.notification

import com.snow.gameservice.config.IntegrationTest
import com.snow.gameservice.config.auth.AuthGenerator
import com.snow.gameservice.config.storage.PopulateAndTeardownDatabase
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.util.JsonObjectMapper
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.nulls.beNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpHeaders
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient
import java.net.URI


@IntegrationTest
class NotificationSocketTest(
    private val dbSetup: PopulateAndTeardownDatabase,
    @LocalServerPort
    private val port: Int,
) : FeatureSpec({
    listeners(dbSetup)

    feature("Listening for notifications") {
        scenario("Single notification should be found") {
            var output: String? = null
            val gameId = dbSetup.notificationId

            (ReactorNettyWebSocketClient().execute(
                URI.create("ws://localhost:$port/notifications"),
                HttpHeaders().apply { setBearerAuth(AuthGenerator.jws) }) { session ->
                session
                    .receive()
                    .take(1)
                    .map { it.payloadAsText }
                    .doOnNext {
                        output = it
                        session.close()
                    }
                    .then()
            }).block()

            gameId shouldNot beNull()
            output?.let { JsonObjectMapper.deserialize<Array<GameId>>(it).first() } shouldBe gameId?.let { GameId(it) }
        }
    }
})