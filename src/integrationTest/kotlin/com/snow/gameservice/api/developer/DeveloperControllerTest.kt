package com.snow.gameservice.api.developer

import com.snow.gameservice.api.game.model.GameDto
import com.snow.gameservice.config.IntegrationTest
import com.snow.gameservice.config.storage.PopulateAndTeardownDatabase
import io.kotest.core.spec.style.FeatureSpec
import org.springframework.test.web.reactive.server.WebTestClient

@IntegrationTest
class DeveloperControllerTest(
    private val webTestClient: WebTestClient,
    private val dbSetup: PopulateAndTeardownDatabase,
) : FeatureSpec({
    listeners(dbSetup)

    feature("Get developers games should return 3 titles") {
        scenario("Should return 3 titles") {
            webTestClient
                .get()
                .uri {
                    it
                        .path("/developers/2137/games")
                        .queryParam("page", 0)
                        .build()
                }
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(GameDto::class.java)
                .hasSize(6)
        }
    }
})