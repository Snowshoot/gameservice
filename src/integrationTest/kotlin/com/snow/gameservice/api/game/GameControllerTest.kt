package com.snow.gameservice.api.game

import com.snow.gameservice.api.game.model.GameDetailsDto
import com.snow.gameservice.api.game.model.GameDto
import com.snow.gameservice.config.IntegrationTest
import com.snow.gameservice.config.storage.PopulateAndTeardownDatabase
import com.snow.gameservice.config.util.bodyInserter
import com.snow.gameservice.game.GameSearchParamsStub
import io.kotest.core.spec.style.FeatureSpec
import org.springframework.test.web.reactive.server.WebTestClient

@IntegrationTest
class GameControllerTest(
    private val webTestClient: WebTestClient,
    private val dbSetup: PopulateAndTeardownDatabase,
) : FeatureSpec({
    listeners(dbSetup)

    feature("Get game info by id") {
        scenario("Should exist") {
            val gameId = dbSetup.gameIds?.first()
            webTestClient
                .get()
                .uri {
                    it
                        .path("/games/$gameId")
                        .build()
                }
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(GameDetailsDto::class.java)
                .hasSize(1)
        }
    }

    feature("Search for games") {
        scenario("Search by query") {
            webTestClient
                .post()
                .uri {
                    it
                        .path("/games")
                        .queryParam("page", 0)
                        .build()
                }
                .body(GameSearchParamsStub.withQuery.bodyInserter())
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(GameDto::class.java)
                .hasSize(6)
        }

        scenario("Search by genres") {
            webTestClient
                .post()
                .uri {
                    it
                        .path("/games")
                        .queryParam("page", 0)
                        .build()
                }
                .body(GameSearchParamsStub.withGenres.bodyInserter())
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(GameDto::class.java)
                .hasSize(6)
        }

        scenario("Search by stores") {
            webTestClient
                .post()
                .uri {
                    it
                        .path("/games")
                        .queryParam("page", 0)
                        .build()
                }
                .body(GameSearchParamsStub.withStores.bodyInserter())
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(GameDto::class.java)
                .hasSize(6)
        }

        scenario("Search exclude games unavailable in stores") {
            webTestClient
                .post()
                .uri {
                    it
                        .path("/games")
                        .queryParam("page", 0)
                        .build()
                }
                .body(GameSearchParamsStub.excludeWithoutStores.bodyInserter())
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(GameDto::class.java)
                .hasSize(6)
        }

        scenario("Search exclude tba") {
            webTestClient
                .post()
                .uri {
                    it
                        .path("/games")
                        .queryParam("page", 0)
                        .build()
                }
                .body(GameSearchParamsStub.excludeTba.bodyInserter())
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(GameDto::class.java)
                .hasSize(6)
        }

        scenario("Search exclude released") {
            webTestClient
                .post()
                .uri {
                    it
                        .path("/games")
                        .queryParam("page", 0)
                        .build()
                }
                .body(GameSearchParamsStub.excludeReleased.bodyInserter())
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(GameDto::class.java)
                .hasSize(5)
        }
    }
})