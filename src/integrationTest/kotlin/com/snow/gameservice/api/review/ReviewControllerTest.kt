package com.snow.gameservice.api.review

import com.snow.gameservice.api.review.model.ReviewDto
import com.snow.gameservice.config.IntegrationTest
import com.snow.gameservice.config.setUser
import com.snow.gameservice.config.storage.PopulateAndTeardownDatabase
import com.snow.gameservice.config.util.bodyInserter
import com.snow.gameservice.review.InputReviewDtoStub
import com.snow.gameservice.user.TestUser
import io.kotest.core.spec.style.FeatureSpec
import org.springframework.test.web.reactive.server.WebTestClient

@IntegrationTest
class ReviewControllerTest(
    private val webTestClient: WebTestClient,
    private val dbSetup: PopulateAndTeardownDatabase,
) : FeatureSpec({
    listeners(dbSetup)
    val curatorClient = webTestClient.setUser(TestUser.curator)
    val userClient = webTestClient.setUser(TestUser.user)

    feature("Get reviews by user") {
        scenario("User has access") {
            curatorClient
                .get()
                .uri {
                    it
                        .path("/reviews")
                        .build()
                }
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(ReviewDto::class.java)
                .hasSize(1)
        }

        scenario("User without access") {
            userClient
                .get()
                .uri {
                    it
                        .path("/reviews")
                        .build()
                }
                .exchange()
                .expectStatus()
                .isForbidden
        }
    }

    feature("Get reviews by game") {
        scenario("Success") {
            val gameId = dbSetup.reviewedId
            webTestClient
                .get()
                .uri {
                    it
                        .path("/reviews/$gameId")
                        .build()
                }
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(ReviewDto::class.java)
                .hasSize(1)
        }
    }

    feature("Add a review") {
        scenario("User has access") {
            val gameId = dbSetup.gameIds?.get(2) ?: throw Exception("Game id should exist")
            curatorClient
                .post()
                .uri {
                    it
                        .path("/reviews")
                        .build()
                }
                .body(
                    InputReviewDtoStub.create(gameId).bodyInserter()
                )
                .exchange()
                .expectStatus()
                .isOk
        }

        scenario("User without access") {
            userClient
                .post()
                .uri {
                    it
                        .path("/reviews")
                        .build()
                }
                .body(
                    InputReviewDtoStub.create("whatever").bodyInserter()
                )
                .exchange()
                .expectStatus()
                .isForbidden
        }
    }

    feature("Delete a review") {
        val gameId = dbSetup.reviewedId
        scenario("User has access") {
            curatorClient
                .delete()
                .uri {
                    it
                        .path("/reviews/$gameId")
                        .build()
                }
                .exchange()
                .expectStatus()
                .isOk
        }

        scenario("User without access") {
            userClient
                .delete()
                .uri {
                    it
                        .path("/reviews/$gameId")
                        .build()
                }
                .exchange()
                .expectStatus()
                .isForbidden
        }
    }
})