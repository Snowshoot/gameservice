package com.snow.gameservice.publisher

import com.snow.gameservice.domain.publisher.model.Publisher

object PublisherStub {
    fun create(
        externalId: Int = 420,
        name: String = "ligma",
    ) =
        Publisher(
            externalId = externalId,
            name = name
        )
}