package com.snow.gameservice.user

import com.snow.gameservice.config.Role

data class TestUser(
    val userId: String,
    val roles: Set<String>,
) {
    companion object {
        val user = TestUser("testuser", setOf(Role.ROLE_USER))
        val curator = TestUser("testcurator", setOf(Role.ROLE_CURATOR))
    }
}