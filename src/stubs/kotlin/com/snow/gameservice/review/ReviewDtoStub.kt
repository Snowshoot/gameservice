package com.snow.gameservice.review

import com.snow.gameservice.api.review.model.InputReviewDto

object InputReviewDtoStub {
    fun create(
        gameId: String,
        rating: Int = 5,
        review: String = "Worst game I've ever played. 10/10",
    ) =
        InputReviewDto(
            gameId = gameId,
            rating = rating,
            review = review
        )
}