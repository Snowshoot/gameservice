package com.snow.gameservice.review

import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.review.model.Rating
import com.snow.gameservice.domain.review.model.Review
import com.snow.gameservice.domain.review.model.ReviewId

object ReviewStub {
    fun create(
        userId: String,
        gameId: GameId,
        rating: Int = 5,
        review: String = "Worst game I've ever played. 10/10",
    ) =
        Review.create(
            reviewId = ReviewId(userId, gameId),
            rating = Rating(rating),
            review = review
        )
}