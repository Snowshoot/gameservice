package com.snow.gameservice.game

import com.snow.gameservice.api.game.model.GameSearchParamsDto
import com.snow.gameservice.genre.GenreStub
import com.snow.gameservice.store.StoreStub

object GameSearchParamsStub {
    fun create(
        query: String = "",
        genres: Set<Int> = setOf(),
        stores: Set<Int> = setOf(),
        excludeWithoutStores: Boolean = false,
        excludeTba: Boolean = false,
        excludeReleased: Boolean = false,
    ) = GameSearchParamsDto(
        query = query,
        genres = genres,
        stores = stores,
        excludeWithoutStores = excludeWithoutStores,
        excludeTba = excludeTba,
        excludeReleased = excludeReleased
    )

    val withQuery = create(query = "Bait")
    val withGenres = create(genres = setOf(GenreStub.create().externalId))
    val withStores = create(stores = setOf(StoreStub.create().externalId))
    val excludeWithoutStores = create(excludeWithoutStores = true)
    val excludeTba = create(excludeTba = true)
    val excludeReleased = create(excludeReleased = true)
}