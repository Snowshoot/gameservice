package com.snow.gameservice.game

import com.snow.gameservice.developer.DeveloperStub
import com.snow.gameservice.domain.developer.model.Developer
import com.snow.gameservice.domain.game.model.ApiGame
import com.snow.gameservice.domain.game.model.ExternalGameId
import com.snow.gameservice.domain.genre.model.Genre
import com.snow.gameservice.domain.publisher.model.Publisher
import com.snow.gameservice.domain.store.model.Store
import com.snow.gameservice.genre.GenreStub
import com.snow.gameservice.publisher.PublisherStub
import com.snow.gameservice.store.StoreStub
import kotlinx.coroutines.flow.flowOf
import java.time.LocalDate

object ApiGameStub {
    fun create(
        externalId: Int,
        title: String,
        description: String = "great game about baiting man",
        genres: Set<Genre> = setOf(GenreStub.create()),
        stores: Set<Store> = setOf(StoreStub.create()),
        developers: Set<Developer> = setOf(DeveloperStub.create()),
        publishers: Set<Publisher> = setOf(PublisherStub.create()),
        released: LocalDate = LocalDate.of(9999, 4, 18),
        tba: Boolean = false,
    ) = ApiGame(
        externalId = ExternalGameId(externalId),
        title = title,
        description = description,
        genres = genres,
        stores = stores,
        developers = developers,
        publishers = publishers,
        coverUrl = "dabjudabjudabjudotgugldotkom",
        released = released,
        tba = tba,
    )

    val collection = flowOf(
        create(1, title = "Baitman1"),
        create(2, title = "Baitman2", released = LocalDate.now()),
        create(3, title = "Baitman3", developers = setOf(), genres = setOf()),
        create(4, title = "Baitman4", publishers = setOf()),
        create(5, title = "Baitman5", stores = setOf()),
        create(6, title = "Baitman6", released = LocalDate.of(1999, 12, 11)),
        create(7, title = "Baltman7 the baldening", tba = true)
    )
}