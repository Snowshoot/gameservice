package com.snow.gameservice.game

import com.snow.gameservice.developer.DeveloperStub
import com.snow.gameservice.domain.developer.model.Developer
import com.snow.gameservice.domain.game.model.ExternalGameId
import com.snow.gameservice.domain.genre.model.Genre
import com.snow.gameservice.domain.publisher.model.Publisher
import com.snow.gameservice.domain.store.model.Store
import com.snow.gameservice.genre.GenreStub
import com.snow.gameservice.messaging.game.receiver.model.GameDataDownloaded
import com.snow.gameservice.publisher.PublisherStub
import com.snow.gameservice.store.StoreStub
import java.time.LocalDate

object GameDataDownloadedStub {
    fun create(
        externalId: Int,
        title: String,
        description: String = "great game about baiting man",
        genres: Set<Genre> = setOf(GenreStub.create()),
        stores: Set<Store> = setOf(StoreStub.create()),
        developers: Set<Developer> = setOf(DeveloperStub.create()),
        publishers: Set<Publisher> = setOf(PublisherStub.create()),
        released: LocalDate = LocalDate.of(9999, 4, 18),
        tba: Boolean = false,
    ) = GameDataDownloaded(
        externalId = ExternalGameId(externalId),
        title = title,
        description = description,
        genres = genres,
        stores = stores,
        developers = developers,
        publishers = publishers,
        coverUrl = "dabjudabjudabjudotgugldotkom",
        released = released,
        tba = tba,
    )
}