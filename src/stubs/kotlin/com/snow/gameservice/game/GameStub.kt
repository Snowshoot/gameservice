package com.snow.gameservice.game

import com.snow.gameservice.developer.DeveloperStub
import com.snow.gameservice.genre.GenreStub
import com.snow.gameservice.publisher.PublisherStub
import com.snow.gameservice.store.StoreStub
import com.snow.gameservice.domain.developer.model.Developer
import com.snow.gameservice.domain.game.model.ExternalGameId
import com.snow.gameservice.domain.game.model.Game
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.genre.model.Genre
import com.snow.gameservice.domain.publisher.model.Publisher
import com.snow.gameservice.domain.store.model.Store
import kotlinx.coroutines.flow.flowOf

object GameStub {
    fun create(
        id: String,
        externalId: Int,
        title: String = "Baitman",
        description: String = "great game about baiting man",
        genres: Set<Genre> = setOf(GenreStub.create()),
        stores: Set<Store> = setOf(StoreStub.create()),
        developers: Set<Developer> = setOf(DeveloperStub.create()),
        publishers: Set<Publisher> = setOf(PublisherStub.create()),
        released: String = "9999-04-18T23:00:00.000+00:00",
        tba: Boolean = false,
    ) = Game(
        id = GameId(id),
        externalId = ExternalGameId(externalId),
        title = title,
        description = description,
        genres = genres,
        stores = stores,
        developers = developers,
        publishers = publishers,
        coverUrl = "dabjudabjudabjudotgugldotkom",
        released = released,
        tba = tba,
    )

    val collection = flowOf(
        create("1234", 1),
        create(">_<", 2),
        create("OwO", 3, developers = setOf()),
        create("UwU",4, publishers = setOf())
    )
}