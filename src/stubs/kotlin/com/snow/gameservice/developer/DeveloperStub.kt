package com.snow.gameservice.developer

import com.snow.gameservice.domain.developer.model.Developer

object DeveloperStub {
    fun create(
        externalId: Int = 2137,
        name: String = "EA Pay",
    ) =
        Developer(
            externalId = externalId,
            name = name
        )
}