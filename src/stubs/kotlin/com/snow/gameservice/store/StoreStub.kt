package com.snow.gameservice.store

import com.snow.gameservice.domain.store.model.Store

object StoreStub {
    fun create(
        externalId: Int = 1234,
        url: String = "kremówka.pl",
        name: String = "kremówka",
    ) = Store(
        externalId = externalId,
        url = url,
        name = name
    )
}