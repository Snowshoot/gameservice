package com.snow.gameservice.genre

import com.snow.gameservice.domain.genre.model.Genre

object GenreStub {
    fun create(
        externalId: Int = 666,
        name: String = "Clicker",
    ) =
        Genre(
            externalId = externalId,
            name = name
        )
}