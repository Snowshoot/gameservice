package com.snow.gameservice.util

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jsonMapper

object JsonObjectMapper {
    inline fun <reified T> serialize(value: T): String = mapper.writeValueAsString(value)
    inline fun <reified T> deserialize(value: String): T = mapper.readValue(value, T::class.java)

    val mapper = jsonMapper {
        this.addModules(
            JavaTimeModule(), KotlinModule.Builder().build()
        )
    }
}