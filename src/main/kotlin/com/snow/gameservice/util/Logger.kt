package com.snow.gameservice.util

import io.github.oshai.kotlinlogging.KotlinLogging

fun logger(name: String) = KotlinLogging.logger(name)

fun <T> T.className() = this?.let { it::class.java.simpleName } ?: throw RuntimeException("Null object")