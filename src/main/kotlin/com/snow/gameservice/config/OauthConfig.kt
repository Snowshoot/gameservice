package com.snow.gameservice.config

import com.nimbusds.jose.shaded.gson.internal.LinkedTreeMap
import com.snow.gameservice.config.Role.ROLE_USER
import com.snow.gameservice.config.Role.SPRING_ROLE_PREFIX
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.security.web.server.SecurityWebFilterChain
import reactor.core.publisher.Mono


@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
class OauthConfig {
    @Bean
    fun securityWebFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain =
        http.apply {
            authorizeExchange {
                it.pathMatchers("/notifications").hasRole(ROLE_USER)
                it.anyExchange().permitAll()
            }
            oauth2ResourceServer {
                it.jwt { customizer ->
                    customizer.jwtAuthenticationConverter(kcAuthConverter())
                }
            }
        }.build()

    fun kcAuthConverter(): Converter<Jwt, Mono<AbstractAuthenticationToken>> =
        Converter<Jwt, Mono<AbstractAuthenticationToken>> { jwt ->
            Mono.just(JwtAuthenticationToken(jwt, jwt.getRoles()))
        }

    private fun Jwt.getRoles() =
        getClaim<Map<String, LinkedTreeMap<String, List<String>>>>("resource_access")["gameservice"]
            ?.get("roles")?.map { SimpleGrantedAuthority("$SPRING_ROLE_PREFIX$it") }
}