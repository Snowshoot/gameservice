package com.snow.gameservice.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.WebSocketHandler


@Configuration
class WebSocketConfig(
    private val webSocketHandler: WebSocketHandler,
) {
    @Bean
    fun handlerMapping(): HandlerMapping =
        SimpleUrlHandlerMapping().apply {
            order = 1
            urlMap = hashMapOf("/notifications" to webSocketHandler)
        }
}
