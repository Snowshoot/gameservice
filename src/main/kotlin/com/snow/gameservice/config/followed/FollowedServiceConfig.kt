package com.snow.gameservice.config.followed

import com.snow.gameservice.domain.followed.FollowedService
import com.snow.gameservice.domain.game.GameService
import com.snow.gameservice.storage.followed.FollowedStorageImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FollowedServiceConfig(
    private val gameService: GameService,
    private val followedStorageImpl: FollowedStorageImpl,
) {
    @Bean
    fun followedService() = FollowedService(
        gameService,
        followedStorageImpl
    )
}