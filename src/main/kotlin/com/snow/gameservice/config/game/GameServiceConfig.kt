package com.snow.gameservice.config.game

import com.snow.gameservice.domain.game.GameService
import com.snow.gameservice.storage.game.GameStorageImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class GameServiceConfig(
    private val gameStorageImpl: GameStorageImpl,
) {
    @Bean
    fun gameService() = GameService(gameStorageImpl)
}