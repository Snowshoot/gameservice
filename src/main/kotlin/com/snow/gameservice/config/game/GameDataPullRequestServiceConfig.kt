package com.snow.gameservice.config.game

import com.snow.gameservice.domain.game.pulling.GameDataPullRequestService
import com.snow.gameservice.messaging.game.publisher.GameDataPullRequestPublisherServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class GameDataPullRequestServiceConfig(
    private val gameDataPullRequestPublisherServiceImpl: GameDataPullRequestPublisherServiceImpl,
) {
    @Bean
    fun gameDataPullRequestService() =
        GameDataPullRequestService(
            gameDataPullRequestPublisherServiceImpl
        )
}