package com.snow.gameservice.config.review

import com.snow.gameservice.domain.game.GameService
import com.snow.gameservice.domain.review.ReviewService
import com.snow.gameservice.storage.review.ReviewStorageImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ReviewServiceConfig(
    private val reviewStorageImpl: ReviewStorageImpl,
    private val gameService: GameService,
) {
    @Bean
    fun reviewService() = ReviewService(reviewStorageImpl, gameService)
}