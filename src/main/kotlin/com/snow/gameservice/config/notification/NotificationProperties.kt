package com.snow.gameservice.config.notification

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "notifications")
data class NotificationProperties(
    val refreshMillis: Long
)