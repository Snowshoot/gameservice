package com.snow.gameservice.config.notification

import com.snow.gameservice.domain.notification.NotificationService
import com.snow.gameservice.storage.notification.NotificationStorageImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class NotificationServiceConfig(
    private val notificationStorageImpl: NotificationStorageImpl,
) {
    @Bean
    fun notificationService() = NotificationService(notificationStorageImpl)
}