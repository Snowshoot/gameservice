package com.snow.gameservice.config

object Role {
    const val ROLE_USER = "user"
    const val ROLE_CURATOR = "curator"
    const val SPRING_ROLE_PREFIX = "ROLE_"
}