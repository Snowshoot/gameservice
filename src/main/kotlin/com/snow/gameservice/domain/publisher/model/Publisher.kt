package com.snow.gameservice.domain.publisher.model

data class Publisher(
    val externalId: Int,
    val name: String,
)