package com.snow.gameservice.domain.followed

import com.snow.gameservice.domain.game.model.GameId
import kotlinx.coroutines.flow.Flow

interface FollowedStorage {
    fun follow(gameId: GameId, userId: String)

    fun unfollow(gameId: GameId, userId: String)

    fun setNotifications(notify: Boolean, userId: String)

    fun findAllByUserId(userId: String, page: Int): Flow<Set<GameId>>

    fun findNotifiableUsersByGameId(gameId: GameId): Flow<String>
}