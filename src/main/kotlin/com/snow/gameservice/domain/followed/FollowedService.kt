package com.snow.gameservice.domain.followed

import com.snow.gameservice.domain.game.GameService
import com.snow.gameservice.domain.game.model.GameId
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flatMapMerge

class FollowedService(
    private val gameService: GameService,
    private val followedStorage: FollowedStorage,
) {
    suspend fun follow(
        gameId: GameId,
        userId: String,
    ) =
        gameService.exists(gameId).collect {
            if (it) followedStorage.follow(gameId, userId)
        }

    fun unfollow(
        gameId: GameId,
        userId: String,
    ) = followedStorage.unfollow(gameId, userId)

    fun setNotifications(
        notify: Boolean,
        userId: String,
    ) = followedStorage.setNotifications(notify, userId)

    @OptIn(ExperimentalCoroutinesApi::class)
    fun findByUserId(
        userId: String,
        page: Int,
    ) =
        followedStorage.findAllByUserId(userId, page).flatMapMerge {
            gameService.findAllByIds(it)
        }

    fun findNotifiableUsersByGameId(
        gameId: GameId,
    ) =
        followedStorage.findNotifiableUsersByGameId(gameId)
}