package com.snow.gameservice.domain.developer.model

data class Developer(
    val externalId: Int,
    val name: String,
)