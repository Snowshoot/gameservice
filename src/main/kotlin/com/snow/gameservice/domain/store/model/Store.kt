package com.snow.gameservice.domain.store.model

data class Store(
    val externalId: Int,
    val url: String,
    val name: String,
)