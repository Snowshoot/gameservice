package com.snow.gameservice.domain.notification

import com.snow.gameservice.domain.game.model.GameId
import kotlinx.coroutines.flow.Flow

interface NotificationStorage {
    fun findNotifications(userId: String): Flow<Set<GameId>>

    suspend fun queueNotifications(gameId: GameId, userIds: Flow<String>)
}