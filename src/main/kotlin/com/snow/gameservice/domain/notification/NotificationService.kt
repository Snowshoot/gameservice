package com.snow.gameservice.domain.notification

import com.snow.gameservice.domain.game.model.GameId
import kotlinx.coroutines.flow.Flow

class NotificationService(
    private val notificationStorage: NotificationStorage,
) {
    fun findNotifications(userId: String): Flow<Set<GameId>> =
        notificationStorage.findNotifications(userId)

    suspend fun queueNotifications(gameId: GameId, userIds: Flow<String>) =
        notificationStorage.queueNotifications(gameId, userIds)
}