package com.snow.gameservice.domain.review.exception

class IncorrectRatingValueException(message: String): RuntimeException(message)