package com.snow.gameservice.domain.review

import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.review.model.Review
import com.snow.gameservice.domain.review.model.ReviewId
import kotlinx.coroutines.flow.Flow

interface ReviewStorage {
    fun save(review: Review)

    fun delete(reviewId: ReviewId)

    fun findAllByUserId(userId: String): Flow<Review>

    fun findAllByGameId(gameId: GameId): Flow<Review>
}