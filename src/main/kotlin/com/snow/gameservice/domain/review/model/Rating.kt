package com.snow.gameservice.domain.review.model

import com.snow.gameservice.domain.review.exception.IncorrectRatingValueException

data class Rating(
    val value: Int
) {
    init {
        if(value !in 1..5)
            throw IncorrectRatingValueException("Rating needs to be between 1 and 5")
    }
}