package com.snow.gameservice.domain.review.model

class Review private constructor(
    val reviewId: ReviewId,
    val rating: Rating,
    val review: String,
) {
    companion object {
        fun create(
            reviewId: ReviewId,
            rating: Rating,
            review: String,
        ): Review =
            Review(
                reviewId = reviewId,
                rating = rating,
                review = review.take(REVIEW_CHAR_LIMIT)
            )

        private const val REVIEW_CHAR_LIMIT = 2000
    }
}