package com.snow.gameservice.domain.review.model

import com.snow.gameservice.domain.game.model.GameId

data class ReviewId(
    val userId: String,
    val gameId: GameId,
)