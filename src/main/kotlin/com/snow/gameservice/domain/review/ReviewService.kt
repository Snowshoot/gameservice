package com.snow.gameservice.domain.review

import com.snow.gameservice.domain.game.GameService
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.review.model.Review
import com.snow.gameservice.domain.review.model.ReviewId

class ReviewService(
    private val reviewStorage: ReviewStorage,
    private val gameService: GameService,
) {
    suspend fun save(review: Review) =
        gameService.exists(review.reviewId.gameId).collect {
            if (it) reviewStorage.save(review)
        }

    fun delete(reviewId: ReviewId) =
        reviewStorage.delete(reviewId)

    fun findAllByUserId(userId: String) =
        reviewStorage.findAllByUserId(userId)

    fun findAllByGameId(gameId: GameId) =
        reviewStorage.findAllByGameId(gameId)
}