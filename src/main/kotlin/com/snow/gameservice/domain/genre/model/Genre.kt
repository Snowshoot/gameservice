package com.snow.gameservice.domain.genre.model

data class Genre(
    val externalId: Int,
    val name: String
)