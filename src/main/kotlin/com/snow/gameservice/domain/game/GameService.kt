package com.snow.gameservice.domain.game

import com.snow.gameservice.domain.game.model.ApiGame
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.game.model.GameSearchParams
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GameService(
    private val gameStorage: GameStorage,
) {
    fun saveAll(games: Flow<ApiGame>) =
        gameStorage.saveAll(games)

    fun findById(id: GameId) =
        gameStorage.findById(id)

    fun findAllByIds(gameIds: Set<GameId>) =
        gameStorage.findAllByIds(gameIds)

    fun findAll(gameSearchParams: GameSearchParams, page: Int) =
        gameStorage.findAll(gameSearchParams, page)

    fun findAllByDeveloperId(id: Int, page: Int) =
        gameStorage.findAllByDeveloperId(id, page)

    fun findAllByPublisherId(id: Int, page: Int) =
        gameStorage.findAllByPublisherId(id, page)

    fun findReleasingToday(): Flow<GameId> =
        gameStorage.findReleasingToday().map { it.id }

    fun exists(id: GameId): Flow<Boolean> =
        gameStorage.exists(id)
}