package com.snow.gameservice.domain.game.model

import com.snow.gameservice.domain.developer.model.Developer
import com.snow.gameservice.domain.genre.model.Genre
import com.snow.gameservice.domain.publisher.model.Publisher
import com.snow.gameservice.domain.store.model.Store

data class Game(
    val id: GameId,
    val externalId: ExternalGameId,
    val title: String,
    val description: String,
    val genres: Set<Genre>,
    val stores: Set<Store>,
    val developers: Set<Developer>,
    val publishers: Set<Publisher>,
    val coverUrl: String?,
    val released: String,
    val tba: Boolean,
)