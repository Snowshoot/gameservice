package com.snow.gameservice.domain.game.model

import com.fasterxml.jackson.annotation.JsonProperty

data class GameId(
    @JsonProperty("value")
    val value: String,
)