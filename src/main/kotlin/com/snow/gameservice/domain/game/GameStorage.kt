package com.snow.gameservice.domain.game

import com.mongodb.client.result.UpdateResult
import com.snow.gameservice.domain.game.model.ApiGame
import com.snow.gameservice.domain.game.model.Game
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.game.model.GameSearchParams
import kotlinx.coroutines.flow.Flow

interface GameStorage {
    fun saveAll(games: Flow<ApiGame>): Flow<UpdateResult>

    fun findById(id: GameId): Flow<Game>

    fun findAllByIds(gameIds: Set<GameId>): Flow<Game>

    fun findAll(gameSearchParams: GameSearchParams, page: Int): Flow<Game>

    fun findAllByDeveloperId(id: Int, page: Int): Flow<Game>

    fun findAllByPublisherId(id: Int, page: Int): Flow<Game>

    fun findReleasingToday(): Flow<Game>

    fun exists(id: GameId): Flow<Boolean>
}