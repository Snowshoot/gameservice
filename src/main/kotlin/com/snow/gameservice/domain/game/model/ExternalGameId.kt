package com.snow.gameservice.domain.game.model

import com.fasterxml.jackson.annotation.JsonProperty

data class ExternalGameId(
    @JsonProperty("value")
    val value: Int,
)