package com.snow.gameservice.domain.game.model

data class GameSearchParams(
    val query: String,
    val genres: Set<Int>,
    val stores: Set<Int>,
    val excludeWithoutStores: Boolean,
    val excludeTba: Boolean,
    val excludeReleased: Boolean,
)