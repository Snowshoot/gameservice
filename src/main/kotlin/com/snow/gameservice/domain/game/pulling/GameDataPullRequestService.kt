package com.snow.gameservice.domain.game.pulling

class GameDataPullRequestService(
    private val gameDataPullRequestPublisherService: GameDataPullRequestPublisherService,
) {
    fun requestPull() =
        gameDataPullRequestPublisherService.publish()
}