package com.snow.gameservice.domain.game.pulling

import kotlinx.coroutines.flow.Flow
import reactor.kafka.sender.SenderResult

interface GameDataPullRequestPublisherService {
    fun publish(): Flow<SenderResult<String?>>
}