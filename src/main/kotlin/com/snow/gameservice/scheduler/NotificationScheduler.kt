package com.snow.gameservice.scheduler

import com.snow.gameservice.domain.followed.FollowedService
import com.snow.gameservice.domain.game.GameService
import com.snow.gameservice.domain.notification.NotificationService
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class NotificationScheduler(
    private val notificationService: NotificationService,
    private val gameService: GameService,
    private val followedService: FollowedService,
) {
    @Scheduled(cron = "@daily")
    suspend fun queueNotifications() {
        gameService.findReleasingToday().map {
            notificationService.queueNotifications(
                gameId = it,
                userIds = followedService.findNotifiableUsersByGameId(it)
            )
        }.collect()
    }
}