package com.snow.gameservice.scheduler

import com.snow.gameservice.domain.game.pulling.GameDataPullRequestService
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class GameDataPullRequestSchedulerService(
    private val gameDataPullRequestService: GameDataPullRequestService,
) {
    @Scheduled(cron = "@midnight")
    fun pullGameData() {
        gameDataPullRequestService.requestPull()
    }
}