package com.snow.gameservice.api.store.model

import com.snow.gameservice.domain.store.model.Store

data class StoreDto(
    val externalId: Int,
    val url: String,
    val name: String,
) {
    constructor(store: Store) : this(
        externalId = store.externalId,
        url = store.url,
        name = store.name
    )
}