package com.snow.gameservice.api.notification

import com.snow.gameservice.config.notification.NotificationProperties
import com.snow.gameservice.util.JsonObjectMapper
import java.time.Duration
import kotlinx.coroutines.reactor.flux
import org.springframework.stereotype.Component
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketMessage
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Component
class NotificationSocket(
    private val notificationApiService: NotificationApiService,
    private val notificationProperties: NotificationProperties,
) : WebSocketHandler {
    override fun handle(session: WebSocketSession): Mono<Void> =
        session.send(
            Flux.interval(Duration.ofMillis(notificationProperties.refreshMillis))
                .flatMap {
                    flux<WebSocketMessage> {
                        notificationApiService.findNotifications().collect { games ->
                            send(session.textMessage(JsonObjectMapper.serialize(games)))
                        }
                    }
                }
        )
}
