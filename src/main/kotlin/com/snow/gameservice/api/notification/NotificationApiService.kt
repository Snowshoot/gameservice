package com.snow.gameservice.api.notification

import com.snow.gameservice.api.auth.AuthApiService
import com.snow.gameservice.domain.notification.NotificationService
import org.springframework.stereotype.Service

@Service
class NotificationApiService(
    private val notificationService: NotificationService,
    private val authApiService: AuthApiService,
) {
    suspend fun findNotifications() =
        authApiService.currentUserId().let {
            notificationService.findNotifications(it)
        }
}