package com.snow.gameservice.api.publisher.model

import com.snow.gameservice.domain.publisher.model.Publisher

data class PublisherDto(
    val externalId: Int,
    val name: String,
) {
    constructor(publisher: Publisher) : this(
        externalId = publisher.externalId,
        name = publisher.name
    )
}