package com.snow.gameservice.api.publisher

import com.snow.gameservice.api.game.GameApiService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController("publishers")
class PublisherController(
    private val gameApiService: GameApiService,
) {
    @GetMapping("/publishers/{id}/games")
    fun findGamesByPublisherId(
        @PathVariable id: Int,
        @RequestParam page: Int,
    ) =
        gameApiService.findAllByPublisherId(id, page)
}