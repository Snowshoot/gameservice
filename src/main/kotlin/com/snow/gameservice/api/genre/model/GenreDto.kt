package com.snow.gameservice.api.genre.model

import com.snow.gameservice.domain.genre.model.Genre

data class GenreDto(
    val externalId: Int,
    val name: String,
) {
    constructor(genre: Genre) : this(
        externalId = genre.externalId,
        name = genre.name
    )
}