package com.snow.gameservice.api.auth

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.asFlow
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Service

@Service
class AuthApiService {
    suspend fun currentUserId(): String =
        ReactiveSecurityContextHolder.getContext().asFlow()
            .map { (it.authentication.principal as Jwt).getClaimAsString("sub") }
            .first()
}