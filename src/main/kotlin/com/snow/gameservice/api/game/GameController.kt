package com.snow.gameservice.api.game

import com.snow.gameservice.api.game.model.GameSearchParamsDto
import org.springframework.web.bind.annotation.*

@RestController("games")
class GameController(
    private val gameApiService: GameApiService,
) {
    @GetMapping("/games/{id}")
    suspend fun findById(
        @PathVariable id: String,
    ) = gameApiService.findById(id)

    @PostMapping("/games")
    fun findAll(
        @RequestBody gameSearchParamsDto: GameSearchParamsDto,
        @RequestParam page: Int,
    ) = gameApiService.findAll(gameSearchParamsDto, page)
}