package com.snow.gameservice.api.game.model

import com.snow.gameservice.domain.game.model.Game
import com.snow.gameservice.domain.game.model.GameId

data class GameDto(
    val id: GameId,
    val title: String,
    val coverUrl: String?,
    val released: String,
    val tba: Boolean,
) {
    constructor(game: Game) : this(
        id = game.id,
        title = game.title,
        coverUrl = game.coverUrl,
        released = game.released,
        tba = game.tba
    )
}