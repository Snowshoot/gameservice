package com.snow.gameservice.api.game

import com.snow.gameservice.api.game.model.GameDetailsDto
import com.snow.gameservice.api.game.model.GameDto
import com.snow.gameservice.api.game.model.GameSearchParamsDto
import com.snow.gameservice.domain.game.GameService
import com.snow.gameservice.domain.game.model.GameId
import kotlinx.coroutines.flow.map
import org.springframework.stereotype.Service

@Service
class GameApiService(
    private val gameService: GameService,
) {
    suspend fun findById(id: String) =
        gameService.findById(GameId(id)).map { GameDetailsDto(it) }

    fun findAll(gameSearchParams: GameSearchParamsDto, page: Int) =
        gameService.findAll(gameSearchParams.toDomain(), page).map { GameDto(it) }

    fun findAllByDeveloperId(id: Int, page: Int) =
        gameService.findAllByDeveloperId(id, page).map { GameDto(it) }

    fun findAllByPublisherId(id: Int, page: Int) =
        gameService.findAllByPublisherId(id, page).map { GameDto(it) }
}

