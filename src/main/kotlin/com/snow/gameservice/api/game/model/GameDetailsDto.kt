package com.snow.gameservice.api.game.model

import com.snow.gameservice.api.developer.model.DeveloperDto
import com.snow.gameservice.api.genre.model.GenreDto
import com.snow.gameservice.api.publisher.model.PublisherDto
import com.snow.gameservice.api.store.model.StoreDto
import com.snow.gameservice.domain.game.model.Game
import com.snow.gameservice.domain.game.model.GameId

data class GameDetailsDto(
    val id: GameId,
    val title: String,
    val description: String,
    val genres: Set<GenreDto>,
    val stores: Set<StoreDto>,
    val developers: Set<DeveloperDto>,
    val publishers: Set<PublisherDto>,
    val coverUrl: String?,
    val released: String,
    val tba: Boolean,
) {
    constructor(game: Game) : this(
        id = game.id,
        title = game.title,
        description = game.description,
        genres = game.genres.map { GenreDto(it) }.toSet(),
        stores = game.stores.map { StoreDto(it) }.toSet(),
        developers = game.developers.map { DeveloperDto(it) }.toSet(),
        publishers = game.publishers.map { PublisherDto(it) }.toSet(),
        coverUrl = game.coverUrl,
        released = game.released,
        tba = game.tba
    )
}