package com.snow.gameservice.api.game.model

import com.snow.gameservice.domain.game.model.GameSearchParams

data class GameSearchParamsDto(
    val query: String = "",
    val genres: Set<Int> = setOf(),
    val stores: Set<Int> = setOf(),
    val excludeWithoutStores: Boolean,
    val excludeTba: Boolean,
    val excludeReleased: Boolean,
) {
    fun toDomain() =
        GameSearchParams(
            query = query,
            genres = genres,
            stores = stores,
            excludeWithoutStores = excludeWithoutStores,
            excludeTba = excludeTba,
            excludeReleased = excludeReleased
        )
}