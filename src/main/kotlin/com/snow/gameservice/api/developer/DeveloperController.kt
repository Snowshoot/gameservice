package com.snow.gameservice.api.developer

import com.snow.gameservice.api.game.GameApiService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController("developers")
class DeveloperController(
    private val gameApiService: GameApiService,
) {
    @GetMapping("/developers/{id}/games")
    fun findGamesByDeveloperId(
        @PathVariable id: Int,
        @RequestParam page: Int,
    ) =
        gameApiService.findAllByDeveloperId(id, page)
}