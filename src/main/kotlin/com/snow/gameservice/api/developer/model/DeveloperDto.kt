package com.snow.gameservice.api.developer.model

import com.snow.gameservice.domain.developer.model.Developer

data class DeveloperDto(
    val externalId: Int,
    val name: String,
) {
    constructor(developer: Developer) : this(
        externalId = developer.externalId,
        name = developer.name
    )
}