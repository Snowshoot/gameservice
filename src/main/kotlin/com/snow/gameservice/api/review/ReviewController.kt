package com.snow.gameservice.api.review

import com.snow.gameservice.api.review.model.InputReviewDto
import com.snow.gameservice.config.Role.ROLE_CURATOR
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@PreAuthorize("hasRole('$ROLE_CURATOR')")
@RestController
class ReviewController(
    private val reviewApiService: ReviewApiService,
) {
    @GetMapping("/reviews")
    suspend fun getByUser() =
        reviewApiService.findAllByUserId()

    @PreAuthorize("permitAll()")
    @GetMapping("/reviews/{gameId}")
    fun getByGame(
        @PathVariable gameId: String,
    ) =
        reviewApiService.findAllByGameId(gameId)

    @PostMapping("/reviews")
    suspend fun save(
        @RequestBody inputReviewDto: InputReviewDto,
    ) = reviewApiService.save(inputReviewDto)

    @DeleteMapping("reviews/{gameId}")
    suspend fun delete(
        @PathVariable gameId: String,
    ) = reviewApiService.delete(gameId)
}