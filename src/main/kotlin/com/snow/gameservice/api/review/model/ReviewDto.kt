package com.snow.gameservice.api.review.model

import com.snow.gameservice.domain.review.model.Review

data class ReviewDto(
    val userId: String,
    val gameId: String,
    val rating: Int,
    val review: String,
) {
    constructor(review: Review) : this(
        userId = review.reviewId.userId,
        gameId = review.reviewId.gameId.value,
        rating = review.rating.value,
        review = review.review
    )
}