package com.snow.gameservice.api.review.model

data class InputReviewDto(
    val gameId: String,
    val rating: Int,
    val review: String,
)