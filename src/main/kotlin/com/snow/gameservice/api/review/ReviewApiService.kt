package com.snow.gameservice.api.review

import com.snow.gameservice.api.auth.AuthApiService
import com.snow.gameservice.api.review.model.InputReviewDto
import com.snow.gameservice.api.review.model.ReviewDto
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.review.ReviewService
import com.snow.gameservice.domain.review.model.Rating
import com.snow.gameservice.domain.review.model.Review
import com.snow.gameservice.domain.review.model.ReviewId
import kotlinx.coroutines.flow.map
import org.springframework.stereotype.Service

@Service
class ReviewApiService(
    private val reviewService: ReviewService,
    private val authApiService: AuthApiService,
) {
    suspend fun save(
        inputReviewDto: InputReviewDto,
    ) =
        authApiService.currentUserId().let {
            reviewService.save(
                Review.create(
                    reviewId = ReviewId(it, GameId(inputReviewDto.gameId)),
                    rating = Rating(inputReviewDto.rating),
                    review = inputReviewDto.review,
                )
            )
        }

    suspend fun delete(gameId: String) =
        authApiService.currentUserId().let {
            reviewService.delete(ReviewId(it, GameId(gameId)))
        }

    fun findAllByGameId(gameId: String) =
        reviewService.findAllByGameId(GameId(gameId)).map { ReviewDto(it) }

    suspend fun findAllByUserId() =
        authApiService.currentUserId().let {
            reviewService.findAllByUserId(it).map { review -> ReviewDto(review) }
        }
}