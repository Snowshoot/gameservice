package com.snow.gameservice.api.followed

import com.snow.gameservice.config.Role.ROLE_USER
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@PreAuthorize("hasRole('$ROLE_USER')")
@RestController("followed")
class FollowedController(
    private val followedApiService: FollowedApiService,
) {
    @GetMapping("/followed")
    suspend fun findByUserId(
        @RequestParam page: Int,
    ) = followedApiService.findByUserId(page)

    @PostMapping("/followed")
    suspend fun follow(
        @RequestParam gameId: String,
    ) = followedApiService.follow(gameId)

    @PostMapping("/followed/notifications")
    suspend fun setNotifications(
        @RequestParam notify: Boolean,
    ) = followedApiService.setNotifications(notify)

    @DeleteMapping("/followed")
    suspend fun unfollow(
        @RequestParam gameId: String,
    ) = followedApiService.unfollow(gameId)
}