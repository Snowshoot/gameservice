package com.snow.gameservice.api.followed

import com.snow.gameservice.api.auth.AuthApiService
import com.snow.gameservice.api.game.model.GameDto
import com.snow.gameservice.domain.followed.FollowedService
import com.snow.gameservice.domain.game.model.GameId
import kotlinx.coroutines.flow.map
import org.springframework.stereotype.Service

@Service
class FollowedApiService(
    private val followedService: FollowedService,
    private val authApiService: AuthApiService,
) {
    suspend fun follow(gameId: String) =
        authApiService.currentUserId().let {
            followedService.follow(GameId(gameId), it)
        }

    suspend fun unfollow(gameId: String) =
        authApiService.currentUserId().let {
            followedService.unfollow(GameId(gameId), it)
        }

    suspend fun setNotifications(notify: Boolean) =
        authApiService.currentUserId().let {
            followedService.setNotifications(notify, it)
        }

    suspend fun findByUserId(page: Int) =
        authApiService.currentUserId().let {
            followedService.findByUserId(it, page).map { gameId -> GameDto(gameId) }
        }
}