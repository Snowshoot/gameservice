package com.snow.gameservice.messaging.game.receiver.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "messaging.games.receiver")
data class GameReceiverProperties(
    val server: String,
    val topic: String,
    val group: String,
)