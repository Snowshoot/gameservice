package com.snow.gameservice.messaging.game.receiver.config

import com.snow.gameservice.messaging.game.receiver.model.GameDataDownloaded
import org.apache.kafka.clients.consumer.ConsumerConfig.*
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.support.serializer.JsonDeserializer
import org.springframework.kafka.support.serializer.JsonDeserializer.TRUSTED_PACKAGES
import org.springframework.kafka.support.serializer.JsonDeserializer.TYPE_MAPPINGS
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverOptions
import java.util.*

@Configuration
class GameReceiverConfig(
    private val properties: GameReceiverProperties,
) {
    @Bean
    fun kafkaReceiver(): KafkaReceiver<String, GameDataDownloaded> =
        KafkaReceiver.create(
            ReceiverOptions.create<String, GameDataDownloaded>(
                mapOf(
                    GROUP_ID_CONFIG to properties.group,
                    BOOTSTRAP_SERVERS_CONFIG to properties.server,
                    KEY_DESERIALIZER_CLASS_CONFIG to StringDeserializer::class.java,
                    VALUE_DESERIALIZER_CLASS_CONFIG to JsonDeserializer::class.java,
                    AUTO_OFFSET_RESET_CONFIG to "earliest",
                    TRUSTED_PACKAGES to PROJECT_CLASSES,
                    TYPE_MAPPINGS to "game:com.snow.gameservice.messaging.game.receiver.model.GameDataDownloaded"
                )
            ).subscription(Collections.singleton(properties.topic))
        )

    companion object {
        const val PROJECT_CLASSES = "com.snow.*"
    }
}