package com.snow.gameservice.messaging.game.publisher.model

import com.fasterxml.jackson.annotation.JsonProperty

data class GameDataPullRequested(
    @JsonProperty("id")
    val id: String,
)