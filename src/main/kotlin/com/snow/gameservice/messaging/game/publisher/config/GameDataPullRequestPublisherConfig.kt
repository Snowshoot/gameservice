package com.snow.gameservice.messaging.game.publisher.config

import com.snow.gameservice.messaging.game.publisher.model.GameDataPullRequested
import org.apache.kafka.clients.producer.ProducerConfig.*
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.support.serializer.JsonDeserializer.TYPE_MAPPINGS
import org.springframework.kafka.support.serializer.JsonSerializer
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions

@Configuration
class GameDataPullRequestPublisherConfig(
    private val properties: GameDataPullRequestPublisherProperties,
) {
    @Bean
    fun sender(): KafkaSender<String, GameDataPullRequested> =
        KafkaSender.create(
            SenderOptions.create(
                mapOf(
                    BOOTSTRAP_SERVERS_CONFIG to properties.server,
                    KEY_SERIALIZER_CLASS_CONFIG to StringSerializer::class.java,
                    VALUE_SERIALIZER_CLASS_CONFIG to JsonSerializer::class.java,
                    TYPE_MAPPINGS to "gameDataPullRequested:com.snow.gameservice.messaging.game.publisher.model.GameDataPullRequested"
                )
            )
        )
}