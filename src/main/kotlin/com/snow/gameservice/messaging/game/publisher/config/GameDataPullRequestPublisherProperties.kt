package com.snow.gameservice.messaging.game.publisher.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "messaging.games.publisher")
data class GameDataPullRequestPublisherProperties(
    val server: String,
    val topic: String,
)