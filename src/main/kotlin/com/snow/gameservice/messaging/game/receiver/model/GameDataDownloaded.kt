package com.snow.gameservice.messaging.game.receiver.model

import com.snow.gameservice.domain.game.model.ExternalGameId
import com.snow.gameservice.domain.game.model.ApiGame
import com.snow.gameservice.domain.developer.model.Developer
import com.snow.gameservice.domain.genre.model.Genre
import com.snow.gameservice.domain.publisher.model.Publisher
import com.snow.gameservice.domain.store.model.Store
import java.time.LocalDate

data class GameDataDownloaded(
    val externalId: ExternalGameId,
    val title: String,
    val description: String,
    val genres: Set<Genre>,
    val stores: Set<Store>,
    val developers: Set<Developer>,
    val publishers: Set<Publisher>,
    val coverUrl: String?,
    val released: LocalDate?,
    val tba: Boolean,
) {
    fun toDomain() =
        ApiGame(
            externalId = this.externalId,
            title = this.title,
            description = this.description,
            genres = this.genres,
            stores = this.stores,
            developers = this.developers,
            publishers = this.publishers,
            coverUrl = this.coverUrl,
            released = this.released,
            tba = this.tba
        )
}