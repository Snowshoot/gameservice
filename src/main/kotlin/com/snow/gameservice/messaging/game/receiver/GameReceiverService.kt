package com.snow.gameservice.messaging.game.receiver

import com.snow.gameservice.domain.game.GameService
import com.snow.gameservice.messaging.game.receiver.model.GameDataDownloaded
import com.snow.gameservice.util.className
import com.snow.gameservice.util.logger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.reactive.asFlow
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import reactor.kafka.receiver.KafkaReceiver

@Service
@Profile("!integrationTest")
class GameReceiverService(
    private val receiver: KafkaReceiver<String, GameDataDownloaded>,
    private val gameService: GameService,
    private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) {
    init {
        coroutineScope.launch { receive() }
    }

    suspend fun receive() =
        receiver.receive()
            .asFlow()
            .apply {
                map {
                    logger(this@GameReceiverService.className()).info {
                        "Received GameDataDownloaded event with external id: ${it.value().externalId}"
                    }
                    it.receiverOffset().acknowledge()
                    it.value().toDomain()
                }.let { gameService.saveAll(it) }.collect()
            }.collect()
}