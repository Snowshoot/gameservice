package com.snow.gameservice.messaging.game.publisher

import com.snow.gameservice.messaging.game.publisher.model.GameDataPullRequested
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderRecord
import reactor.kafka.sender.SenderResult

@Component
class GameDataPullRequestPublisher(
    private val sender: KafkaSender<String, GameDataPullRequested>,
) {
    fun publish(request: Mono<SenderRecord<String, GameDataPullRequested, String?>>): Flux<SenderResult<String?>> =
        sender.send(request).also { it.subscribe() }
}