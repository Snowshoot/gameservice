package com.snow.gameservice.messaging.game.publisher

import com.snow.gameservice.domain.game.pulling.GameDataPullRequestPublisherService
import com.snow.gameservice.messaging.game.publisher.config.GameDataPullRequestPublisherProperties
import com.snow.gameservice.messaging.game.publisher.model.GameDataPullRequested
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactive.asFlow
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kafka.sender.SenderRecord
import reactor.kafka.sender.SenderResult
import java.util.*

@Service
class GameDataPullRequestPublisherServiceImpl(
    private val gameDataPullRequestPublisher: GameDataPullRequestPublisher,
    private val properties: GameDataPullRequestPublisherProperties,
) : GameDataPullRequestPublisherService {
    override fun publish(): Flow<SenderResult<String?>> =
        Mono.just(
            SenderRecord.create(
                properties.topic,
                0,
                null,
                "${UUID.randomUUID()}",
                GameDataPullRequested("${UUID.randomUUID()}"),
                ""
            )
        ).let { gameDataPullRequestPublisher.publish(it) }
            .asFlow()
}