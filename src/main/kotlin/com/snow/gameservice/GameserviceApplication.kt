package com.snow.gameservice

import com.snow.gameservice.config.notification.NotificationProperties
import com.snow.gameservice.messaging.game.publisher.config.GameDataPullRequestPublisherProperties
import com.snow.gameservice.messaging.game.receiver.config.GameReceiverProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(
    GameReceiverProperties::class,
    GameDataPullRequestPublisherProperties::class,
    NotificationProperties::class
)
class GameserviceApplication

fun main(args: Array<String>) {
    runApplication<GameserviceApplication>(*args)
}