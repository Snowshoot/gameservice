package com.snow.gameservice.storage.developer.model

import com.snow.gameservice.domain.developer.model.Developer
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document("developers")
data class DeveloperDocument(
    @Id
    val id: String = UUID.randomUUID().toString(),
    val externalId: Int,
    val name: String,
) {
    constructor(developer: Developer) : this(
        externalId = developer.externalId,
        name = developer.name
    )

    fun toDomain() =
        Developer(
            externalId = this.externalId,
            name = this.name
        )
}