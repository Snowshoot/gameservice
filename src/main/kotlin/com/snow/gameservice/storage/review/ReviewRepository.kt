package com.snow.gameservice.storage.review

import com.snow.gameservice.storage.review.model.ReviewDocument
import com.snow.gameservice.storage.review.model.ReviewIdDocument
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ReviewRepository : ReactiveMongoRepository<ReviewDocument, ReviewIdDocument>