package com.snow.gameservice.storage.review

import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.review.ReviewStorage
import com.snow.gameservice.domain.review.model.Review
import com.snow.gameservice.domain.review.model.ReviewId
import com.snow.gameservice.storage.review.model.ReviewDocument
import com.snow.gameservice.storage.review.model.ReviewIdDocument
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.asFlow
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Service

@Service
class ReviewStorageImpl(
    private val reviewRepository: ReviewRepository,
    private val mongoTemplate: ReactiveMongoTemplate,
) : ReviewStorage {
    override fun save(review: Review) {
        reviewRepository.save(ReviewDocument(review, review.reviewId.userId)).subscribe()
    }

    override fun delete(reviewId: ReviewId) {
        mongoTemplate.remove(
            Query(Criteria.where("_id").`is`(ReviewIdDocument(reviewId.userId, reviewId.gameId))),
            ReviewDocument::class.java
        ).subscribe()
    }

    override fun findAllByUserId(userId: String): Flow<Review> =
        mongoTemplate.find(
            Query(Criteria.where("_id.userId").`is`(userId)),
            ReviewDocument::class.java
        ).asFlow().map { it.toDomain() }

    override fun findAllByGameId(gameId: GameId): Flow<Review> =
        mongoTemplate.find(
            Query(Criteria.where("_id.gameId").`is`(gameId)),
            ReviewDocument::class.java
        ).asFlow().map { it.toDomain() }
}