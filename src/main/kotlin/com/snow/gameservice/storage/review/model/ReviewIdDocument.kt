package com.snow.gameservice.storage.review.model

import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.review.model.ReviewId
import org.springframework.data.mongodb.core.mapping.Document

@Document("reviewId")
data class ReviewIdDocument(
    val userId: String,
    val gameId: GameId,
) {
    fun toDomain() =
        ReviewId(
            userId,
            gameId
        )
}