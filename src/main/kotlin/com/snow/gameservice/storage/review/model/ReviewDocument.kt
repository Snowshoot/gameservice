package com.snow.gameservice.storage.review.model

import com.snow.gameservice.domain.review.model.Rating
import com.snow.gameservice.domain.review.model.Review
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("reviews")
data class ReviewDocument(
    @Id
    val id: ReviewIdDocument,
    val rating: Rating,
    val review: String,
) {
    constructor(review: Review, userId: String) : this(
        id = ReviewIdDocument(userId, review.reviewId.gameId),
        rating = review.rating,
        review = review.review,
    )

    fun toDomain() =
        Review.create(
            reviewId = id.toDomain(),
            rating = rating,
            review = review
        )
}