package com.snow.gameservice.storage.followed.model

import com.snow.gameservice.domain.game.model.GameId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("followed")
data class FollowedDocument(
    @Id
    val userId: String,
    val notify: Boolean,
    val games: Set<GameId>,
)