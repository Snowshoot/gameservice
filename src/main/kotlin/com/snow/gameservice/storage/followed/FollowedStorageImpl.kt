package com.snow.gameservice.storage.followed

import com.snow.gameservice.domain.followed.FollowedStorage
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.storage.followed.model.FollowedDocument
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.asFlow
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Service

@Service
class FollowedStorageImpl(
    private val mongoTemplate: ReactiveMongoTemplate,
) : FollowedStorage {
    override fun follow(gameId: GameId, userId: String) {
        mongoTemplate.upsert(
            Query(Criteria.where("_id").`is`(userId)),
            Update()
                .setOnInsert("notify", true)
                .addToSet("games", gameId),
            FollowedDocument::class.java
        ).subscribe()
    }

    override fun unfollow(gameId: GameId, userId: String) {
        mongoTemplate.upsert(
            Query(Criteria.where("_id").`is`(userId)),
            Update().pull("games", gameId),
            FollowedDocument::class.java
        ).subscribe()
    }

    override fun setNotifications(notify: Boolean, userId: String) {
        mongoTemplate.upsert(
            Query(Criteria.where("_id").`is`(userId)),
            Update().set("notify", notify),
            FollowedDocument::class.java
        ).subscribe()
    }

    override fun findAllByUserId(userId: String, page: Int): Flow<Set<GameId>> =
        mongoTemplate.find(
            Query(Criteria.where("_id").`is`(userId)).with(PageRequest.of(page, PAGE_SIZE)),
            FollowedDocument::class.java
        ).asFlow().map { it.games }

    override fun findNotifiableUsersByGameId(gameId: GameId): Flow<String> =
        mongoTemplate.find(
            Query(
                Criteria.where("notify").`is`(true).andOperator(
                    Criteria.where("games").elemMatch(Criteria.where("value").`is`(gameId.value))
                )
            ),
            FollowedDocument::class.java
        ).asFlow().map { it.userId }

    companion object {
        private const val PAGE_SIZE = 20
    }
}