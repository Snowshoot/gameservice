package com.snow.gameservice.storage.genre.model

import com.snow.gameservice.domain.genre.model.Genre
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document("genres")
data class GenreDocument(
    @Id
    val id: String = UUID.randomUUID().toString(),
    val externalId: Int,
    val name: String,
) {
    constructor(genre: Genre) : this(
        externalId = genre.externalId,
        name = genre.name
    )

    fun toDomain() =
        Genre(
            externalId = this.externalId,
            name = this.name
        )
}