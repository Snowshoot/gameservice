package com.snow.gameservice.storage.notification

import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.notification.NotificationStorage
import com.snow.gameservice.storage.notification.model.NotificationDocument
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.asFlow
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Service

@Service
@OptIn(ExperimentalCoroutinesApi::class)
class NotificationStorageImpl(
    private val mongoTemplate: ReactiveMongoTemplate,
) : NotificationStorage {
    override fun findNotifications(userId: String): Flow<Set<GameId>> =
        mongoTemplate.find(
            Query(Criteria.where("_id").`is`(userId)),
            NotificationDocument::class.java
        ).asFlow().map { it.games }

    override suspend fun queueNotifications(gameId: GameId, userIds: Flow<String>) {
        userIds.flatMapMerge {
            mongoTemplate.upsert(
                Query(Criteria.where("_id").`is`(it)),
                Update().addToSet("games", gameId),
                NotificationDocument::class.java
            ).asFlow()
        }.collect()
    }
}