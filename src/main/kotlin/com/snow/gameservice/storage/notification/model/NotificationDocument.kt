package com.snow.gameservice.storage.notification.model

import com.snow.gameservice.domain.game.model.GameId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("notification")
data class NotificationDocument(
    @Id
    val userId: String,
    val games: Set<GameId>,
)