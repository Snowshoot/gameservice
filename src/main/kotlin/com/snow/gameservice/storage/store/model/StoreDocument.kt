package com.snow.gameservice.storage.store.model

import com.snow.gameservice.domain.store.model.Store
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document("stores")
data class StoreDocument(
    @Id
    val id: String = UUID.randomUUID().toString(),
    val externalId: Int,
    val url: String,
    val name: String,
) {
    constructor(store: Store) : this(
        externalId = store.externalId,
        url = store.url,
        name = store.name
    )

    fun toDomain() =
        Store(
            externalId = this.externalId,
            url = this.url,
            name = this.name
        )
}