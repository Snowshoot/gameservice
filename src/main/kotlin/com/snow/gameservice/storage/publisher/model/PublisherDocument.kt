package com.snow.gameservice.storage.publisher.model

import com.snow.gameservice.domain.publisher.model.Publisher
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document("publishers")
data class PublisherDocument(
    @Id
    val id: String = UUID.randomUUID().toString(),
    val externalId: Int,
    val name: String,
) {
    constructor(publisher: Publisher) : this(
        externalId = publisher.externalId,
        name = publisher.name
    )

    fun toDomain() =
        Publisher(
            externalId = this.externalId,
            name = this.name
        )
}