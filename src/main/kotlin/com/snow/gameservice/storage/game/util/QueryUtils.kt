package com.snow.gameservice.storage.game.util

import com.snow.gameservice.domain.game.model.GameSearchParams
import com.snow.gameservice.storage.game.model.GameDocument
import com.snow.gameservice.storage.store.model.StoreDocument
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import java.time.LocalDate
import java.util.*
import java.util.regex.Pattern

fun GameSearchParams.createQuery() =
    Query().apply {
        if (query.isNotBlank())
            addCriteria(Criteria.where("title").regex(Pattern.compile(".*$query.*", Pattern.CASE_INSENSITIVE)))
        if (genres.isNotEmpty())
            addCriteria(Criteria.where("genres").elemMatch(Criteria.where("externalId").`in`(genres)))
        if (stores.isNotEmpty())
            addCriteria(Criteria.where("stores").elemMatch(Criteria.where("externalId").`in`(stores)))
        if (excludeWithoutStores)
            addCriteria(Criteria.where("stores").ne(Collections.emptySet<StoreDocument>()))
        if(excludeTba)
            addCriteria(Criteria.where("tba").`is`(false))
        if (excludeReleased)
            addCriteria(Criteria.where("released").gt(LocalDate.now()))
    }

fun GameDocument.createUpdate() =
    Update().apply {
        setOnInsert("_id", id)
        set(GameDocument::title.name, title)
        set(GameDocument::description.name, description)
        set(GameDocument::genres.name, genres)
        set(GameDocument::stores.name, stores)
        set(GameDocument::developers.name, developers)
        set(GameDocument::publishers.name, publishers)
        set(GameDocument::coverUrl.name, coverUrl)
        set(GameDocument::released.name, released)
        set(GameDocument::tba.name, tba)
    }
