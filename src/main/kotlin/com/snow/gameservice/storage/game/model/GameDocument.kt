package com.snow.gameservice.storage.game.model

import com.snow.gameservice.domain.game.model.ExternalGameId
import com.snow.gameservice.domain.game.model.ApiGame
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.game.model.Game
import com.snow.gameservice.storage.developer.model.DeveloperDocument
import com.snow.gameservice.storage.genre.model.GenreDocument
import com.snow.gameservice.storage.publisher.model.PublisherDocument
import com.snow.gameservice.storage.store.model.StoreDocument
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

@Document("games")
data class GameDocument(
    @Id
    val id: String = UUID.randomUUID().toString(),
    @Indexed(unique = true)
    val externalId: ExternalGameId,
    val title: String,
    val description: String,
    val genres: Set<GenreDocument>,
    val stores: Set<StoreDocument>,
    val developers: Set<DeveloperDocument>,
    val publishers: Set<PublisherDocument>,
    val coverUrl: String?,
    val released: LocalDate?,
    val tba: Boolean,
) {
    constructor(game: ApiGame) : this(
        externalId = game.externalId,
        title = game.title,
        description = game.description,
        genres = game.genres.map { GenreDocument(it) }.toSet(),
        stores = game.stores.map { StoreDocument(it) }.toSet(),
        developers = game.developers.map { DeveloperDocument(it) }.toSet(),
        publishers = game.publishers.map { PublisherDocument(it) }.toSet(),
        coverUrl = game.coverUrl,
        released = game.released,
        tba = game.tba
    )

    fun toDomain() =
        Game(
            id = GameId(this.id),
            externalId = this.externalId,
            title = this.title,
            description = this.description,
            genres = this.genres.map { it.toDomain() }.toSet(),
            stores = this.stores.map { it.toDomain() }.toSet(),
            developers = this.developers.map { it.toDomain() }.toSet(),
            publishers = this.publishers.map { it.toDomain() }.toSet(),
            coverUrl = this.coverUrl,
            released = this.released?.let { DateTimeFormatter.ofPattern("dd.MM.yyyy").format(it) } ?: "",
            tba = this.tba
        )
}