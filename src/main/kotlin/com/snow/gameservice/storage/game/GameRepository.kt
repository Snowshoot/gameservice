package com.snow.gameservice.storage.game

import com.snow.gameservice.storage.game.model.GameDocument
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

@Repository
interface GameRepository : ReactiveMongoRepository<GameDocument, String>