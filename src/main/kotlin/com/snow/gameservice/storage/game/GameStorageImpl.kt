package com.snow.gameservice.storage.game

import com.mongodb.client.result.UpdateResult
import com.snow.gameservice.domain.game.GameStorage
import com.snow.gameservice.domain.game.model.ApiGame
import com.snow.gameservice.domain.game.model.Game
import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.domain.game.model.GameSearchParams
import com.snow.gameservice.storage.game.model.GameDocument
import com.snow.gameservice.storage.game.util.createQuery
import com.snow.gameservice.storage.game.util.createUpdate
import java.time.LocalDate
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.asFlow
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Service

@Service
class GameStorageImpl(
    private val gameRepository: GameRepository,
    private val mongoTemplate: ReactiveMongoTemplate,
) : GameStorage {
    override fun findById(id: GameId): Flow<Game> =
        gameRepository.findById(id.value).asFlow().map { it.toDomain() }

    override fun findAllByIds(gameIds: Set<GameId>): Flow<Game> =
        mongoTemplate.find(
            Query(Criteria.where("_id").`in`(gameIds.map { it.value })),
            GameDocument::class.java
        ).asFlow().map { it.toDomain() }

    override fun findAllByDeveloperId(id: Int, page: Int): Flow<Game> =
        mongoTemplate.find(
            Query(
                Criteria.where("developers").elemMatch(Criteria.where("externalId").`is`(id))
            ).with(PageRequest.of(page, PAGE_SIZE)),
            GameDocument::class.java
        ).asFlow().map { it.toDomain() }

    override fun findAllByPublisherId(id: Int, page: Int): Flow<Game> =
        mongoTemplate.find(
            Query(
                Criteria.where("publishers").elemMatch(Criteria.where("externalId").`is`(id))
            ).with(PageRequest.of(page, PAGE_SIZE)),
            GameDocument::class.java
        ).asFlow().map { it.toDomain() }

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun saveAll(games: Flow<ApiGame>): Flow<UpdateResult> =
        games.flatMapMerge {
            mongoTemplate.upsert(
                Query(Criteria.where("externalId").`is`(it.externalId)),
                GameDocument(it).createUpdate(),
                GameDocument::class.java
            ).asFlow()
        }

    override fun findAll(
        gameSearchParams: GameSearchParams,
        page: Int,
    ): Flow<Game> =
        mongoTemplate.find(
            gameSearchParams.createQuery().with(PageRequest.of(page, PAGE_SIZE)),
            GameDocument::class.java
        ).asFlow().map { it.toDomain() }

    override fun findReleasingToday(): Flow<Game> =
        with(LocalDate.now().atStartOfDay()) {
            mongoTemplate.find(
                Query(Criteria.where("released").gte(this).lte(this.plusDays(1))),
                GameDocument::class.java
            ).asFlow().map { it.toDomain() }
        }

    override fun exists(id: GameId): Flow<Boolean> =
        mongoTemplate.exists(
            Query(Criteria.where("_id").`is`(id.value)),
            GameDocument::class.java
        ).asFlow()

    companion object {
        private const val PAGE_SIZE = 50
    }
}