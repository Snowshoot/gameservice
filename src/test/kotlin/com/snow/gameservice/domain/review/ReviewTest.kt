package com.snow.gameservice.domain.review

import com.snow.gameservice.domain.game.model.GameId
import com.snow.gameservice.review.ReviewStub
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.shouldBe

class ReviewTest : FeatureSpec({
    feature("Creating a review") {
        scenario("Review over 2000 characters should be cut") {
            ReviewStub.create(
                userId = "testuser",
                gameId = GameId("somegame"),
                review = (1..2024).joinToString(";")
            ).review.length shouldBe 2000
        }
    }
})