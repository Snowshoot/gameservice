package com.snow.gameservice.util

import com.snow.gameservice.domain.game.model.ApiGame
import com.snow.gameservice.game.ApiGameStub
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.shouldBe

class JsonObjectMapperTest : FeatureSpec({
    val serialized =
        "{\"externalId\":{\"value\":1234},\"title\":\"Not fifa 2033\",\"description\":\"great game about baiting man\",\"genres\":[{\"externalId\":666,\"name\":\"Clicker\"}],\"stores\":[{\"externalId\":1234,\"url\":\"kremówka.pl\",\"name\":\"kremówka\"}],\"developers\":[{\"externalId\":2137,\"name\":\"EA Pay\"}],\"publishers\":[{\"externalId\":420,\"name\":\"ligma\"}],\"coverUrl\":\"dabjudabjudabjudotgugldotkom\",\"released\":[9999,4,18],\"tba\":false}"
    val deserialized = ApiGameStub.create(externalId = 1234, title = "Not fifa 2033")

    feature("Serializing an object with time fields") {
        scenario("Should work") {
            JsonObjectMapper.serialize(deserialized) shouldBe serialized
        }
    }

    feature("Deserializing an object with time fields") {
        scenario("Should work") {
            JsonObjectMapper.deserialize<ApiGame>(serialized) shouldBe deserialized
        }
    }
})