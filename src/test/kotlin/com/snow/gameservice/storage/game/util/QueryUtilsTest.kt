package com.snow.gameservice.storage.game.util

import com.snow.gameservice.game.ApiGameStub
import com.snow.gameservice.storage.game.model.GameDocument
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.shouldBe
import kotlin.reflect.full.memberProperties

class QueryUtilsTest : FeatureSpec({
    feature("Create game update params") {
        scenario("All params except id and externalId should be marked for modification") {
            val game = GameDocument(ApiGameStub.create(4321, "bingus adventures"))
            val update = game.createUpdate()

            GameDocument::class.memberProperties
                .map {
                    if (it.name == "id") "_id"
                    else it.name
                }
                .filter { it != "externalId" }
                .map { update.modifies(it) }
                .all { it } shouldBe true
        }
    }
})