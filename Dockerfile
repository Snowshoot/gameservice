FROM gradle:8.5 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle bootJar

FROM eclipse-temurin:21-jdk-alpine
RUN addgroup -S rungroup && adduser -S apprunner -G rungroup
USER apprunner
WORKDIR /app
COPY --from=build /home/gradle/src/build/libs/*.jar ./app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]